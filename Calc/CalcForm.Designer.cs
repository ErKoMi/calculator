﻿
namespace CalcWinForms
{
    partial class CalcForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalcForm));
            this.but_num_7 = new System.Windows.Forms.Button();
            this.but_num_8 = new System.Windows.Forms.Button();
            this.but_num_9 = new System.Windows.Forms.Button();
            this.but_num_6 = new System.Windows.Forms.Button();
            this.but_num_5 = new System.Windows.Forms.Button();
            this.but_num_4 = new System.Windows.Forms.Button();
            this.but_num_3 = new System.Windows.Forms.Button();
            this.but_num_2 = new System.Windows.Forms.Button();
            this.but_num_1 = new System.Windows.Forms.Button();
            this.but_num_0 = new System.Windows.Forms.Button();
            this.but_op_sep = new System.Windows.Forms.Button();
            this.but_change = new System.Windows.Forms.Button();
            this.labelOut = new System.Windows.Forms.Label();
            this.but_op_equ = new System.Windows.Forms.Button();
            this.but_op_sum = new System.Windows.Forms.Button();
            this.but_op_sub = new System.Windows.Forms.Button();
            this.but_op_mul = new System.Windows.Forms.Button();
            this.but_op_div = new System.Windows.Forms.Button();
            this.but_op_clear = new System.Windows.Forms.Button();
            this.but_backspace = new System.Windows.Forms.Button();
            this.labelState = new System.Windows.Forms.Label();
            this.but_History = new System.Windows.Forms.Button();
            this.historyList = new System.Windows.Forms.ListBox();
            this.but_clearHistory = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // but_num_7
            // 
            this.but_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_7.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_7.Location = new System.Drawing.Point(9, 193);
            this.but_num_7.Name = "but_num_7";
            this.but_num_7.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_7.Size = new System.Drawing.Size(95, 95);
            this.but_num_7.TabIndex = 7;
            this.but_num_7.TabStop = false;
            this.but_num_7.Tag = "7";
            this.but_num_7.Text = "7";
            this.but_num_7.UseVisualStyleBackColor = false;
            this.but_num_7.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_8
            // 
            this.but_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_8.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_8.Location = new System.Drawing.Point(110, 193);
            this.but_num_8.Name = "but_num_8";
            this.but_num_8.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_8.Size = new System.Drawing.Size(95, 95);
            this.but_num_8.TabIndex = 8;
            this.but_num_8.TabStop = false;
            this.but_num_8.Tag = "8";
            this.but_num_8.Text = "8";
            this.but_num_8.UseVisualStyleBackColor = false;
            this.but_num_8.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_9
            // 
            this.but_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_9.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_9.Location = new System.Drawing.Point(211, 193);
            this.but_num_9.Name = "but_num_9";
            this.but_num_9.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_9.Size = new System.Drawing.Size(95, 95);
            this.but_num_9.TabIndex = 9;
            this.but_num_9.TabStop = false;
            this.but_num_9.Tag = "9";
            this.but_num_9.Text = "9";
            this.but_num_9.UseVisualStyleBackColor = false;
            this.but_num_9.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_6
            // 
            this.but_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_6.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_6.Location = new System.Drawing.Point(211, 294);
            this.but_num_6.Name = "but_num_6";
            this.but_num_6.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_6.Size = new System.Drawing.Size(95, 95);
            this.but_num_6.TabIndex = 6;
            this.but_num_6.TabStop = false;
            this.but_num_6.Tag = "6";
            this.but_num_6.Text = "6";
            this.but_num_6.UseVisualStyleBackColor = false;
            this.but_num_6.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_5
            // 
            this.but_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_5.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_5.Location = new System.Drawing.Point(110, 294);
            this.but_num_5.Name = "but_num_5";
            this.but_num_5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_5.Size = new System.Drawing.Size(95, 95);
            this.but_num_5.TabIndex = 5;
            this.but_num_5.TabStop = false;
            this.but_num_5.Tag = "5";
            this.but_num_5.Text = "5";
            this.but_num_5.UseVisualStyleBackColor = false;
            this.but_num_5.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_4
            // 
            this.but_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_4.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_4.Location = new System.Drawing.Point(9, 294);
            this.but_num_4.Name = "but_num_4";
            this.but_num_4.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_4.Size = new System.Drawing.Size(95, 95);
            this.but_num_4.TabIndex = 4;
            this.but_num_4.TabStop = false;
            this.but_num_4.Tag = "4";
            this.but_num_4.Text = "4";
            this.but_num_4.UseVisualStyleBackColor = false;
            this.but_num_4.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_3
            // 
            this.but_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_3.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_3.Location = new System.Drawing.Point(211, 395);
            this.but_num_3.Name = "but_num_3";
            this.but_num_3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_3.Size = new System.Drawing.Size(95, 95);
            this.but_num_3.TabIndex = 3;
            this.but_num_3.TabStop = false;
            this.but_num_3.Tag = "3";
            this.but_num_3.Text = "3";
            this.but_num_3.UseVisualStyleBackColor = false;
            this.but_num_3.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_2
            // 
            this.but_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_2.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_2.Location = new System.Drawing.Point(110, 395);
            this.but_num_2.Name = "but_num_2";
            this.but_num_2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_2.Size = new System.Drawing.Size(95, 95);
            this.but_num_2.TabIndex = 2;
            this.but_num_2.TabStop = false;
            this.but_num_2.Tag = "2";
            this.but_num_2.Text = "2";
            this.but_num_2.UseVisualStyleBackColor = false;
            this.but_num_2.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_1
            // 
            this.but_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_1.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_1.Location = new System.Drawing.Point(9, 395);
            this.but_num_1.Name = "but_num_1";
            this.but_num_1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_1.Size = new System.Drawing.Size(95, 95);
            this.but_num_1.TabIndex = 20;
            this.but_num_1.TabStop = false;
            this.but_num_1.Tag = "1";
            this.but_num_1.Text = "1";
            this.but_num_1.UseVisualStyleBackColor = false;
            this.but_num_1.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_num_0
            // 
            this.but_num_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_num_0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_num_0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_num_0.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_num_0.Location = new System.Drawing.Point(110, 496);
            this.but_num_0.Name = "but_num_0";
            this.but_num_0.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_num_0.Size = new System.Drawing.Size(95, 95);
            this.but_num_0.TabIndex = 12;
            this.but_num_0.TabStop = false;
            this.but_num_0.Tag = "0";
            this.but_num_0.Text = "0";
            this.but_num_0.UseVisualStyleBackColor = false;
            this.but_num_0.Click += new System.EventHandler(this.ButNum_Click);
            // 
            // but_op_sep
            // 
            this.but_op_sep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.but_op_sep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_op_sep.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_op_sep.Location = new System.Drawing.Point(211, 496);
            this.but_op_sep.Name = "but_op_sep";
            this.but_op_sep.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_op_sep.Size = new System.Drawing.Size(95, 95);
            this.but_op_sep.TabIndex = 11;
            this.but_op_sep.TabStop = false;
            this.but_op_sep.Tag = "sep";
            this.but_op_sep.Text = ",";
            this.but_op_sep.UseVisualStyleBackColor = false;
            this.but_op_sep.Click += new System.EventHandler(this.but_op_sep_Click);
            // 
            // but_change
            // 
            this.but_change.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.but_change.BackgroundImage = global::CalcWinForms.Properties.Resources.plus_minus;
            this.but_change.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_change.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_change.Location = new System.Drawing.Point(9, 496);
            this.but_change.Name = "but_change";
            this.but_change.Size = new System.Drawing.Size(95, 95);
            this.but_change.TabIndex = 10;
            this.but_change.TabStop = false;
            this.but_change.Tag = "change";
            this.but_change.UseVisualStyleBackColor = false;
            this.but_change.Click += new System.EventHandler(this.but_change_Click);
            // 
            // labelOut
            // 
            this.labelOut.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.labelOut.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelOut.Location = new System.Drawing.Point(9, 103);
            this.labelOut.Name = "labelOut";
            this.labelOut.Size = new System.Drawing.Size(496, 87);
            this.labelOut.TabIndex = 1;
            this.labelOut.Text = "0";
            this.labelOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // but_op_equ
            // 
            this.but_op_equ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.but_op_equ.BackgroundImage = global::CalcWinForms.Properties.Resources.premium_icon_equals_5521806;
            this.but_op_equ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_op_equ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_op_equ.Location = new System.Drawing.Point(312, 496);
            this.but_op_equ.Name = "but_op_equ";
            this.but_op_equ.Size = new System.Drawing.Size(196, 95);
            this.but_op_equ.TabIndex = 14;
            this.but_op_equ.TabStop = false;
            this.but_op_equ.Tag = "equation";
            this.but_op_equ.UseVisualStyleBackColor = false;
            this.but_op_equ.Click += new System.EventHandler(this.But_equ_Click);
            // 
            // but_op_sum
            // 
            this.but_op_sum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.but_op_sum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_op_sum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_op_sum.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_op_sum.Location = new System.Drawing.Point(413, 395);
            this.but_op_sum.Name = "but_op_sum";
            this.but_op_sum.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_op_sum.Size = new System.Drawing.Size(95, 95);
            this.but_op_sum.TabIndex = 18;
            this.but_op_sum.TabStop = false;
            this.but_op_sum.Tag = "";
            this.but_op_sum.Text = "+";
            this.but_op_sum.UseVisualStyleBackColor = false;
            this.but_op_sum.Click += new System.EventHandler(this.ButOp_Click);
            // 
            // but_op_sub
            // 
            this.but_op_sub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.but_op_sub.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_op_sub.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_op_sub.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_op_sub.Location = new System.Drawing.Point(312, 395);
            this.but_op_sub.Name = "but_op_sub";
            this.but_op_sub.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_op_sub.Size = new System.Drawing.Size(95, 95);
            this.but_op_sub.TabIndex = 19;
            this.but_op_sub.TabStop = false;
            this.but_op_sub.Tag = "";
            this.but_op_sub.Text = "-";
            this.but_op_sub.UseVisualStyleBackColor = false;
            this.but_op_sub.Click += new System.EventHandler(this.ButOp_Click);
            // 
            // but_op_mul
            // 
            this.but_op_mul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.but_op_mul.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_op_mul.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_op_mul.Font = new System.Drawing.Font("Segoe UI Historic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_op_mul.Location = new System.Drawing.Point(415, 294);
            this.but_op_mul.Name = "but_op_mul";
            this.but_op_mul.Padding = new System.Windows.Forms.Padding(5, 0, 0, 15);
            this.but_op_mul.Size = new System.Drawing.Size(95, 95);
            this.but_op_mul.TabIndex = 20;
            this.but_op_mul.TabStop = false;
            this.but_op_mul.Tag = "";
            this.but_op_mul.Text = "x";
            this.but_op_mul.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.but_op_mul.UseVisualStyleBackColor = false;
            this.but_op_mul.Click += new System.EventHandler(this.ButOp_Click);
            // 
            // but_op_div
            // 
            this.but_op_div.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.but_op_div.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_op_div.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_op_div.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_op_div.Location = new System.Drawing.Point(312, 294);
            this.but_op_div.Name = "but_op_div";
            this.but_op_div.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.but_op_div.Size = new System.Drawing.Size(95, 95);
            this.but_op_div.TabIndex = 21;
            this.but_op_div.TabStop = false;
            this.but_op_div.Tag = "";
            this.but_op_div.Text = "/";
            this.but_op_div.UseVisualStyleBackColor = false;
            this.but_op_div.Click += new System.EventHandler(this.ButOp_Click);
            // 
            // but_op_clear
            // 
            this.but_op_clear.BackColor = System.Drawing.Color.Red;
            this.but_op_clear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_op_clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_op_clear.Font = new System.Drawing.Font("Stencil", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_op_clear.Location = new System.Drawing.Point(413, 193);
            this.but_op_clear.Name = "but_op_clear";
            this.but_op_clear.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_op_clear.Size = new System.Drawing.Size(95, 95);
            this.but_op_clear.TabIndex = 22;
            this.but_op_clear.TabStop = false;
            this.but_op_clear.Tag = "clear";
            this.but_op_clear.Text = "c";
            this.but_op_clear.UseVisualStyleBackColor = false;
            this.but_op_clear.Click += new System.EventHandler(this.but_op_clear_Click);
            // 
            // but_backspace
            // 
            this.but_backspace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.but_backspace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.but_backspace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_backspace.Font = new System.Drawing.Font("Stencil", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_backspace.Location = new System.Drawing.Point(312, 193);
            this.but_backspace.Name = "but_backspace";
            this.but_backspace.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.but_backspace.Size = new System.Drawing.Size(95, 95);
            this.but_backspace.TabIndex = 23;
            this.but_backspace.TabStop = false;
            this.but_backspace.Tag = "backspace";
            this.but_backspace.Text = "<-";
            this.but_backspace.UseVisualStyleBackColor = false;
            this.but_backspace.Click += new System.EventHandler(this.but_backspace_Click);
            // 
            // labelState
            // 
            this.labelState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelState.Font = new System.Drawing.Font("Segoe UI", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelState.Location = new System.Drawing.Point(9, 50);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(496, 44);
            this.labelState.TabIndex = 24;
            this.labelState.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // but_History
            // 
            this.but_History.Font = new System.Drawing.Font("Stencil", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.but_History.Location = new System.Drawing.Point(460, 5);
            this.but_History.Name = "but_History";
            this.but_History.Size = new System.Drawing.Size(50, 42);
            this.but_History.TabIndex = 25;
            this.but_History.TabStop = false;
            this.but_History.Text = "🕗";
            this.but_History.UseVisualStyleBackColor = true;
            this.but_History.Click += new System.EventHandler(this.but_History_Click);
            // 
            // historyList
            // 
            this.historyList.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.historyList.FormattingEnabled = true;
            this.historyList.ItemHeight = 25;
            this.historyList.Location = new System.Drawing.Point(527, 12);
            this.historyList.Name = "historyList";
            this.historyList.Size = new System.Drawing.Size(343, 529);
            this.historyList.TabIndex = 26;
            this.historyList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.historyList_MouseDoubleClick);
            // 
            // but_clearHistory
            // 
            this.but_clearHistory.Location = new System.Drawing.Point(527, 554);
            this.but_clearHistory.Name = "but_clearHistory";
            this.but_clearHistory.Size = new System.Drawing.Size(343, 28);
            this.but_clearHistory.TabIndex = 27;
            this.but_clearHistory.Text = "🗑";
            this.but_clearHistory.UseVisualStyleBackColor = true;
            this.but_clearHistory.Click += new System.EventHandler(this.but_clearHistory_Click);
            // 
            // CalcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(882, 598);
            this.Controls.Add(this.but_clearHistory);
            this.Controls.Add(this.historyList);
            this.Controls.Add(this.but_History);
            this.Controls.Add(this.labelState);
            this.Controls.Add(this.but_backspace);
            this.Controls.Add(this.but_op_clear);
            this.Controls.Add(this.but_op_div);
            this.Controls.Add(this.but_op_mul);
            this.Controls.Add(this.but_op_sub);
            this.Controls.Add(this.but_op_sum);
            this.Controls.Add(this.but_op_equ);
            this.Controls.Add(this.labelOut);
            this.Controls.Add(this.but_change);
            this.Controls.Add(this.but_op_sep);
            this.Controls.Add(this.but_num_0);
            this.Controls.Add(this.but_num_3);
            this.Controls.Add(this.but_num_2);
            this.Controls.Add(this.but_num_1);
            this.Controls.Add(this.but_num_6);
            this.Controls.Add(this.but_num_5);
            this.Controls.Add(this.but_num_4);
            this.Controls.Add(this.but_num_9);
            this.Controls.Add(this.but_num_8);
            this.Controls.Add(this.but_num_7);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(900, 645);
            this.MinimumSize = new System.Drawing.Size(540, 645);
            this.Name = "CalcForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Калькулятор";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CalcForm_KeyPress);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button but_num_7;
        private System.Windows.Forms.Button but_num_8;
        private System.Windows.Forms.Button but_num_9;
        private System.Windows.Forms.Button but_num_6;
        private System.Windows.Forms.Button but_num_5;
        private System.Windows.Forms.Button but_num_4;
        private System.Windows.Forms.Button but_num_3;
        private System.Windows.Forms.Button but_num_2;
        private System.Windows.Forms.Button but_num_1;
        private System.Windows.Forms.Button but_num_0;
        private System.Windows.Forms.Button but_op_sep;
        private System.Windows.Forms.Button but_change;
        private System.Windows.Forms.Label labelOut;
        private System.Windows.Forms.Button but_op_equ;
        private System.Windows.Forms.Button but_op_sum;
        private System.Windows.Forms.Button but_op_sub;
        private System.Windows.Forms.Button but_op_mul;
        private System.Windows.Forms.Button but_op_div;
        private System.Windows.Forms.Button but_op_clear;
        private System.Windows.Forms.Button but_backspace;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Button but_History;
        private System.Windows.Forms.ListBox historyList;
        private System.Windows.Forms.Button but_clearHistory;
    }
}

