﻿using System;
using System.Windows.Forms;
using System.Drawing;
using CalcLib;

namespace CalcWinForms
{
    public partial class CalcForm : Form
    {

        public CalcForm()
        {
            InitializeComponent();
            but_op_sep.Text = separator.ToString();
            but_op_sum.Tag = Computing.Operations.sum;
            but_op_sub.Tag = Computing.Operations.sub;
            but_op_mul.Tag = Computing.Operations.mul;
            but_op_div.Tag = Computing.Operations.div;
            Width = MinimumSize.Width;
        }
        private Font OutLabelDefaultFont = new("Stencil", 48);
        private Font OutLabelSmallFont = new("Stencil", 30);

        private char separator = ',';

        private Computing calculator = new(); //Создание калькулятора


        /// <summary>
        /// Флаг ввода нового числа
        /// </summary>
        private bool newNumber = false;
        /// <summary>
        /// Флаг изменения числа
        /// </summary>
        private bool change_digit = false;
        /// <summary>
        /// Флаг отчистки
        /// </summary>
        private bool f_reset = false;
        /// <summary>
        /// Флаг открытия окна с историей
        /// </summary>
        private bool f_history = false;


        /// <summary>
        /// вывод цифры на экран
        /// </summary>
        private void OutDigit(string digit)
        {
            if (newNumber) //Если нужно ввести новое число - отчистить вывод
            {
                labelOut.Text = "";
                newNumber = false;
            }
                
            if (labelOut.Text.Length >= 8)
                return;
            if ((labelOut.Text == "0" || labelOut.Text == "-0") && digit != ",")
            {
                if (labelOut.Text == "-0")
                    labelOut.Text = "-"; 
                else
                    labelOut.Text = "";
            }
            
            labelOut.Text += digit;
        }

        /// <summary>
        /// Вывод числа на экран
        /// </summary>
        /// <param name="num"></param>
        private void OutNumber(double num)
        {
            if (num.ToString().Length > 15)
            {
                labelOut.Font = OutLabelSmallFont;
                labelOut.Text = num.ToString("E4");
                return;
            }
            if (num.ToString().Length > 9)
            {
                labelOut.Font = OutLabelSmallFont;
                labelOut.Text = num.ToString();
            }
            else
            {
                labelOut.Font = OutLabelDefaultFont;
                labelOut.Text = num.ToString();
            }
            
        }
        
        //Обработка нажатий кнопок формы

        /// <summary>
        /// Обработка нажатия цифровой кнопки
        /// </summary>
        private void ButNum_Click(object sender, EventArgs e)
        {
            InputDigit((sender as Button).Tag.ToString());
        }

        /// <summary>
        /// Обработка нажатия кнопки математематической операции
        /// </summary>
        private void ButOp_Click(object sender, EventArgs e)
        {
            InputOperation((Computing.Operations)(sender as Button).Tag);
        }

        /// <summary>
        /// Обработка нажатия кнопки "равно"
        /// </summary>
        private void But_equ_Click(object sender, EventArgs e)
        {
            InputEqu();
        }

        /// <summary>
        /// Обработка кнопки отчистки последнего знака
        /// </summary>
        private void but_backspace_Click(object sender, EventArgs e)
        {
            Backspace();
        }

        /// <summary>
        /// Обработка нажатия кнопки смены знака
        /// </summary>
        private void but_change_Click(object sender, EventArgs e)
        {
            ChangeSign();
        }

        /// <summary>
        /// Обработка нажатия кнопки разделителя
        /// </summary>
        private void but_op_sep_Click(object sender, EventArgs e)
        {
            InputSeparator();
        }

        /// <summary>
        /// Обработчик нажатия кнопки сброса
        /// </summary>
        private void but_op_clear_Click(object sender, EventArgs e)
        {
            Reset();
        }

      

        //Обработка нажатий на клавиатуре

        private void CalcForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                InputDigit(e.KeyChar.ToString());
            }
            
            if (e.KeyChar == '+')
            {
                InputOperation(Computing.Operations.sum);
            }
            if (e.KeyChar == '-')
            {
                InputOperation(Computing.Operations.sub);
            }
            if (e.KeyChar == '*')
            {
                InputOperation(Computing.Operations.mul);
            }
            if (e.KeyChar == '/')
            {
                InputOperation(Computing.Operations.div);
            }

            if (e.KeyChar == ',')
            {
                InputSeparator();
            }
            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == '=')
            {
                InputEqu();
            }
            if (e.KeyChar == (char)Keys.Back)
            {
                Backspace();
            }
            if (e.KeyChar == (char)Keys.Escape || e.KeyChar == (char)Keys.Delete)
            {
                Reset();
            }

        }


        //Логика работы программы

        /// <summary>
        /// Обработка ввода цифры
        /// </summary>
        /// <param name="digit"></param>
        private void InputDigit(string digit)
        {
            ChangeOpButtonsEnable(true);

            if (f_reset) //Сброс если взведен флаг сброса
            {
                Reset();
            }
            OutDigit(digit);

            change_digit = true; //число было изменено
        }
        /// <summary>
        /// обработка ввода операции
        /// </summary>
        /// <param name="operation"></param>
        private void InputOperation(Computing.Operations operation)
        {
            try
            {
                f_reset = false; //Пользователь решил продолжить вычисления - сброс отменяется

                /*Отправка в калькулятор данных о тексте на экране, типе операции и изменении числа
                 и вывод на экран результата обработки данных*/
                OutNumber(calculator.InputOperation(double.Parse(labelOut.Text),
                    operation, change_digit));
                

                change_digit = false; //число при нажатии кнопки операции не изменяется
                newNumber = true; //Программа готова к вводу нового числа

                labelState.Text = calculator.UpdateStateString(); //Обновление строки операции
                string h_rec = calculator.GetHistoryRecord();
                if (h_rec != "")
                    historyList.Items.Insert(0, h_rec); //Внесение непустой записи в историю
            }
            catch (Exception)
            {
                ExeptionHandler();
            }


        }
        /// <summary>
        /// Обработка ввода разделителя
        /// </summary>
        private void InputSeparator()
        {
            if (f_reset) //Сброс если взведен флаг сброса
            {
                Reset();
            }
            if (labelOut.Text.Contains(separator)) //Если разделитель уже есть в числе - возврат
                return;
            if (!newNumber)
                OutDigit(separator.ToString());
            else  //Если флаг ввода нового числа - добавляем ноль перед разделителем 
                OutDigit("0" + separator.ToString());
        }
        /// <summary>
        /// Обработка смены знака числа
        /// </summary>
        private void ChangeSign()
        {

            if (labelOut.Text[0] == '-') //Если минус уже есть - убираем
                labelOut.Text = labelOut.Text.Substring(1);
            else //Если минуса нет - ставим
                labelOut.Text = '-' + labelOut.Text;

            if (f_reset)
                calculator.ChangeFirstOperandSign();

            change_digit = true; //число было изменено
            newNumber = false;

        }
        /// <summary>
        /// Обработка удаления последнего символа числа
        /// </summary>
        private void Backspace()
        {
            if (f_reset)
            {
                return; //Если выставлен флаг сброса - удалить последний символ нельзя
            }
            labelOut.Text = labelOut.Text.Substring(0, labelOut.Text.Length - 1);
            if (labelOut.Text.Length == 0 || labelOut.Text == "-")
                labelOut.Text = "0";
        }
        /// <summary>
        /// Обработка "Равно"
        /// </summary>
        private void InputEqu()
        {
            try
            {
                //Передача в калькулятор данных о тексте вывода и вывод результата на экран
                OutNumber(calculator.Equation(double.Parse(labelOut.Text)));
                labelState.Text = calculator.UpdateStateString(); //Обновление строки операции
                change_digit = false; //Число не изменено
                f_reset = true; //Сброс
                string h_rec = calculator.GetHistoryRecord();
                if (h_rec != "")
                    historyList.Items.Insert(0, h_rec); //Внесение непустой записи в историю
            }
            catch (Exception)
            {
                ExeptionHandler();
            }
        }
        /// <summary>
        /// Сбрасывает состояние калькулятора на исходное
        /// </summary>
        private void Reset()
        {
            ChangeOpButtonsEnable(true);
            OutNumber(0);
            calculator.Reset();
            labelState.Text = calculator.UpdateStateString();
            f_reset = false;
            newNumber = true;
        }
        /// <summary>
        /// Обработчик исключений
        /// </summary>
        private void ExeptionHandler()
        {
            labelOut.Font = OutLabelDefaultFont;
            labelOut.Text = "Ошибка!";
            ChangeOpButtonsEnable(false);
            f_reset = true;
        }

        /// <summary>
        /// Изменение доступности кнопок операций
        /// </summary>
        /// <param name="f"></param>
        private void ChangeOpButtonsEnable(bool f)
        {
            but_op_sum.Enabled = f;
            but_op_sub.Enabled = f;
            but_op_mul.Enabled = f;
            but_op_div.Enabled = f;
            but_op_equ.Enabled = f;
            but_backspace.Enabled = f;
            but_change.Enabled = f;
            but_op_sep.Enabled = f;
        }
        /// <summary>
        /// Нажатие на кнопку истории
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void but_History_Click(object sender, EventArgs e)
        {
            if (f_history)
            {
                Width = MinimumSize.Width;
            }
            else
            {
                Width = MaximumSize.Width;
            }
            f_history = !f_history;
        }
        /// <summary>
        /// Нажатие на кнопку отчистки истории
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void but_clearHistory_Click(object sender, EventArgs e)
        {
            calculator.ClearHistory();
            historyList.Items.Clear();
        }
        /// <summary>
        /// Загрузка вычисления из истории
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void historyList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = historyList.IndexFromPoint(e.Location);
            labelOut.Text = calculator.LoadHistory(index);
            labelState.Text = calculator.UpdateStateString();
            ChangeOpButtonsEnable(true);
            change_digit = false; //Число не изменено
            f_reset = true; //Сброс

        }
    }
}
