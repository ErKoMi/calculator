﻿using System;
using System.Collections.Generic;

namespace CalcLib
{
    /// <summary>
    /// Класс, реализующий вычисления на калькуляторе
    /// </summary>
    public class Computing
    {
        private double operand_1;
        private double operand_2;


        /// <summary>
        /// Флаг ввода 1 операнда
        /// </summary>
        private bool operand_1IsEnter = false;
        /// <summary>
        /// Флаг ввода 2 операнда
        /// </summary>
        private bool operand_2IsEnter = false;

        private OperationScreen statestring = new();
        private HistoryRecord record = new();

        private Operations operation = Operations.none;

        private List<HistoryRecord> HistoryRecords = new List<HistoryRecord> { };

        public Operations Operation { get => operation; private set => operation = value; }
        public double Operand_1
        {
            get { return operand_1; }
            private set
            {
                operand_1 = value;
                operand_1IsEnter = true;
            }
        }
        public double Operand_2
        {
            get { return operand_2; }
            private set
            {
                operand_2 = value;
                operand_2IsEnter = true;
            }
        }
        
        public enum Operations
        {
            none, sum, sub, mul, div
        }

        public static string OperationToString(Computing.Operations operation)
        {
            switch (operation)
            {
                case Computing.Operations.none:
                    return "";
                case Computing.Operations.sum:
                    return " + ";
                case Computing.Operations.sub:
                    return " - ";
                case Computing.Operations.mul:
                    return " x ";
                case Computing.Operations.div:
                    return " / ";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Получение данных при смене операции
        /// </summary>
        /// <param name="num">текцщее число в выводе</param>
        /// <param name="op">тип операции</param>
        /// <param name="change">изменялось ли число</param>
        /// <returns>число, которое должно быть в выводе</returns>
        public double InputOperation(double num, Operations op, bool change)
        {
            //Число не менялось - вычислять не надо, нужно только сменить операцию
            if (!change) 
            {
                operand_2IsEnter = false;
                Operand_1 = num;
            }
            else
            {
                if (!operand_2IsEnter && operand_1IsEnter) //Ожидается ввод 2 операнда
                {
                    Operand_2 = num; //вводим 2 операнд
                    Calculate(); //Вычисляем
                    operand_2IsEnter = false; //ожидаем ввод 2 операнда
                }

                if (!operand_1IsEnter) //Ожидается ввод 1 операнда
                {
                    Operand_1 = num; //вводим 1 операнд
                }
            }
            Operation = op; //Запоминаем операцию
            statestring.FirstOperand = Operand_1.ToString(); //Обновляем первый операнд в строке состояния
            return Operand_1; //Результат преобразований - в 1 операнде
        }

        /// <summary>
        /// Смена знака первого операнда
        /// </summary>
        public void ChangeFirstOperandSign()
        {
            Operand_1 *= -1;
        }


        /// <summary>
        /// Получение результатов вычисления
        /// </summary>
        /// <param name="num">текущее число в выводе</param>
        /// <returns>число, которое должно быть в выводе</returns>
        public double Equation(double num)
        {
            if (Operation == Operations.none) //Если функция вызвана сразу после создания калькулятора
            {
                record = new();
                Operand_1 = num; //Запоминаем первый операнд
                record.Result = record.FirstOperand = Operand_1.ToString(); //Заносим запись в историю
                record.Update();
                return Operand_1;
            }

            /*Если ожидается ввод второго операнда или второй операнд никогда не был введен*/
            if(!operand_2IsEnter) 
            {
                Operand_2 = num; //Запоминаем второй операнд
            }
            statestring.FirstOperand = operand_1.ToString();
            return Calculate(); //Возвращаем результат
        }

        /// <summary>
        /// Вычисление в зависимости от операции, результат - в первом операнде
        /// </summary>
        /// <returns>результат вычислений</returns>
        public double Calculate()
        {
            record = new();
            record.FirstOperand = Operand_1.ToString();
            record.SecondOperand = Operand_2.ToString();
            record.Operation = operation;
            switch (Operation)
            {
                case Operations.sum:
                    Operand_1 += Operand_2;
                    break;
                case Operations.sub:
                    Operand_1 -= Operand_2;
                    break;
                case Operations.mul:
                    Operand_1 *= Operand_2;
                    break;
                case Operations.div:
                    if(Operand_2 == 0)
                    {
                        throw new DivideByZeroException();
                    }
                    Operand_1 /= Operand_2;
                    break;
            }
            record.Result = Operand_1.ToString();
            record.Update();
            return Operand_1;
        }

        /// <summary>
        /// Сброс калькулятора в дефолтное состояние
        /// </summary>
        public void Reset()
        {
            operand_1IsEnter = operand_2IsEnter = false;
            Operation = Operations.none;
            statestring.FirstOperand = "";
        }
        
        public string UpdateStateString()
        {
            if (operand_2IsEnter)
                statestring.SecondOperand = Operand_2.ToString();
            else
                statestring.SecondOperand = "";
            statestring.Operation = Operation;
            return statestring.Update();
        }

        public string GetHistoryRecord()
        {
            string h_rec = record.GetRecord();
            if(h_rec != "")
            {
                HistoryRecords.Insert(0, record);
            }
            return h_rec;
        }

        /// <summary>
        /// Загрузка вычисления из истории
        /// </summary>
        /// <param name="index">номер в истории</param>
        /// <returns>результат вычисления</returns>
        public string LoadHistory(int index)
        {
            Reset();
            if (index < 0 || index > HistoryRecords.Count)
                return "0";
            HistoryRecord rec = HistoryRecords[index];
            if(rec.FirstOperand != "")
            {
                Operand_1 = double.Parse(rec.Result);
                statestring.FirstOperand = rec.FirstOperand;
                operand_1IsEnter = true;
            }
            if (rec.Operation != Operations.none)
            {
                Operation = rec.Operation;
                statestring.Operation = Operation;
            }
            if (rec.SecondOperand != "")
            {
                Operand_2 = double.Parse(rec.SecondOperand);
                statestring.SecondOperand = rec.FirstOperand;
            }
            statestring.Update();
            return rec.Result;
        }

        public void ClearHistory()
        {
            HistoryRecords.Clear();
        }
    }
}
