﻿namespace CalcLib
{
    /// <summary>
    /// Класс для строки операции
    /// </summary>
    public class OperationScreen
    {
        private string firstOperand = "";
        private string secondOperand = "";
        private Computing.Operations operation = Computing.Operations.none;

        private string OutString = "";

        public Computing.Operations Operation { get => operation; set => operation = value; }

        public string FirstOperand
        {
            get { return firstOperand; }
            set
            {
                if(value.Length > 14)
                {
                    value = double.Parse(value).ToString("E4");
                }
                firstOperand = value;
            }
        }

        public string SecondOperand
        {
            get { return secondOperand; }
            set
            {
                if (value.Length > 14)
                {
                    value = double.Parse(value).ToString("E1");
                }
                secondOperand = value;
            }
        }





        /// <summary>
        /// Обновляет выходную строку
        /// </summary>
        /// <returns>Обновленная строка вывода</returns>
        public string Update()
        {
            
            OutString = FirstOperand + Computing.OperationToString(Operation) + SecondOperand;
            return OutString;
        }
    }
}
