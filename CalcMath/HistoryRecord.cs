﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcLib
{
    /// <summary>
    /// Класс для хранения вычисления
    /// </summary>
    public class HistoryRecord : OperationScreen
    {
        private string result = "";
        private string record = "";
        private bool f_read = true;
        
        public string Result
        {
            get { return result; }
            set
            {
                if (value.Length > 14)
                {
                    value = double.Parse(value).ToString("E1");
                }
                result = value;
            }
        }

        public string Record
        {
            get
            {
                return record;
            }
            set
            {
                record = value;
            }
        }

        /// <summary>
        /// Возвращает запись один раз, в последствии возвращает пустую строку
        /// </summary>
        /// <returns></returns>
        public string GetRecord()
        {
            if (f_read)
            {
                f_read = false;
                return record;
            }
            return "";
        }

        public new void Update()
        {
            Record = base.Update() + " =\n" + result;
        }


    }
}
